# GenerateObjects4Devs
 - Este é um projeto criado em meu tempo livre, e tem como principal objetivo exemplificar a forma de utilização do framework 
 RestAssured para automatização de testes de APIs. Como eu não possuo uma API, utilizei as do site www.4Devs.com.br. Site que,
 eu geralmente utilizo, para gerar minhas passas de dados.
 
 - Neste projeto você também vai encontrar exemplos de gerar objetos do response, que podem ser utilizados como massa de dados
 em projetos de automação.

## Como utilizar
 - Para utilizar o projeto como ferramenta de geração de massa de dados, basta realizar o download,
 realizar o build e utilizar o .jar que será gerado.

 - Para ter como base para a criação de automação de APIs, basta realizar o clone do projeto e abrir
 com a IDE de sua preferência para consultar os exemplos de testes verificar como funciona.
 
### Configurações Necessárias
 - IDE de sua preferência;
 - Java JDK 11;
 - Gradle;
 - Lombok (Plugin para IDE);
 - Habilitar a opção 'Annotation Processor' nas configurações de sua IDE;

   OBS.: No meu caso estou utilizando a IDE IntelliJ, então inseri no arquivo .gitignore apenas as configurações para esta IDE. 
    
#### Execução
 - É possivel realizar a execução dos testes abrindo o projeto em uma IDE de sua preferência ou acessando
  o diretório do projeto, através do gerenciador de arquivos de seu computador e rodar o comando "gradle clean test". 
package br.com.fordevs.objects;

import lombok.Data;
import lombok.Builder;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class PessoaObj {

    String nome;
    String cpf;
    String rg;
    String dataNascimento;
    String signo;
    String nomeMae;
    String nomePai;
    String email;
    String cep;
    String rua;
    int numero;
    String bairro;
    String cidade;
    String uf;
    String telefone;
    String celular;
}

package br.com.fordevs.objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class DocumentosPessoaObj {

    String Cpf;
    String Rg;
    String TituloEleitor;
    String PisPasep;
}

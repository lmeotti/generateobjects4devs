package br.com.fordevs.specifications;

import br.com.fordevs.environment.LoaderProperties;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.parsing.Parser;
import io.restassured.specification.RequestSpecification;

public class RequestSpec {

    public static RequestSpecification GetSpecPessoaPost(){
        return new RequestSpecBuilder()
                        .setRelaxedHTTPSValidation()
                        .setBaseUri(LoaderProperties.getBaseUri())
                        .setBasePath(LoaderProperties.getBasePathUsers())
                        .setContentType("application/x-www-form-urlencoded")
                        .addParam("acao", LoaderProperties.getParamAcaoPessoa())
                    .build()
        ;
    }

    public static RequestSpecification GetSpecCpfPost(){
        return new RequestSpecBuilder()
                .setRelaxedHTTPSValidation()
                .setBaseUri(LoaderProperties.getBaseUri())
                .setBasePath(LoaderProperties.getBasePathUsers())
                .setContentType("application/x-www-form-urlencoded")
                .addParam("acao", LoaderProperties.getParamAcaoCpf())
                .build()
                ;
    }

    public static RequestSpecification GetSpecRgPost(){
        return new RequestSpecBuilder()
                .setRelaxedHTTPSValidation()
                .setBaseUri(LoaderProperties.getBaseUri())
                .setBasePath(LoaderProperties.getBasePathUsers())
                .setContentType("application/x-www-form-urlencoded")
                .addParam("acao", LoaderProperties.getParamAcaoRg())
                .build()
                ;
    }

    public static RequestSpecification GetSpecCnpjPost() {
        return new RequestSpecBuilder()
                .setRelaxedHTTPSValidation()
                .setBaseUri(LoaderProperties.getBaseUri())
                .setBasePath(LoaderProperties.getBasePathUsers())
                .setContentType("application/x-www-form-urlencoded")
                .addParam("acao", LoaderProperties.getParamAcaoCnpj())
                .build()
                ;
    }

    public static RequestSpecification GetSpecTituloEleitorPost() {
        return new RequestSpecBuilder()
                .setRelaxedHTTPSValidation()
                .setBaseUri(LoaderProperties.getBaseUri())
                .setBasePath(LoaderProperties.getBasePathUsers())
                .setContentType("application/x-www-form-urlencoded")
                .addParam("acao", LoaderProperties.getParamAcaoTituloEleitor())
                .build()
                ;
    }

    public static RequestSpecification GetSpecPisPasepPost() {
        return new RequestSpecBuilder()
                .setRelaxedHTTPSValidation()
                .setBaseUri(LoaderProperties.getBaseUri())
                .setBasePath(LoaderProperties.getBasePathUsers())
                .setContentType("application/x-www-form-urlencoded")
                .addParam("acao", LoaderProperties.getParamAcaoPisPasep())
                .build()
                ;
    }
}

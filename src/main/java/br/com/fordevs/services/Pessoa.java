package br.com.fordevs.services;

import br.com.fordevs.objects.PessoaObj;
import br.com.fordevs.specifications.RequestSpec;
import br.com.fordevs.utils.GeneroPessoa;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.*;

public class Pessoa  {

    public static PessoaObj Get(GeneroPessoa genero , Boolean comPontuacao, Integer idade){
        //TODO Implementar lista de pessoas
//        if(quantidadePessoas > 30) {
//            throw new IllegalArgumentException("Quantidade máxima de Pessoas deve ser 30.");
//        } else {
            JsonPath response = given()
                    .spec(RequestSpec.GetSpecPessoaPost())
                .when()
                    .param("sexo", genero)
                    .param("pontuacao", comPontuacao ? "S" : "N")
                    .param("idade", idade)
                    .param("txt_qtde", 1)
                    .post()
                .then()
                    .statusCode(200)
                    .contentType("application/json")
                    .extract()
                    .body()
                    .jsonPath()
            ;

            return PessoaObj
                    .builder()
                        .nome(response.getString("nome"))
                        .cpf(response.getString("cpf"))
                        .rg(response.getString("rg"))
                        .dataNascimento(response.getString("data_nasc"))
                        .signo(response.getString("signo"))
                        .nomeMae(response.getString("mae"))
                        .nomePai(response.getString("pai"))
                        .email(response.getString("nome").trim().concat("mailinator.com"))
                        .cep(response.getString("cep"))
                        .rua(response.getString("endereco"))
                        .numero(Integer.parseInt(response.getString("numero")))
                        .bairro(response.getString("bairro"))
                        .cidade(response.getString("cidade"))
                        .uf(response.getString("estado"))
                        .telefone(response.getString("telefone_fixo"))
                        .celular(response.getString("celular"))
                    .build()
            ;
//        }
    }
}

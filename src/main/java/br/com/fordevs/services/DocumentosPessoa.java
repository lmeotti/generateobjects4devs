package br.com.fordevs.services;

import br.com.fordevs.objects.DocumentosPessoaObj;
import br.com.fordevs.specifications.RequestSpec;
import br.com.fordevs.utils.Estados;
import groovy.json.JsonParser;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.registerParser;

public class DocumentosPessoa {

    public static DocumentosPessoaObj Get(Boolean comPontuacao, Estados estado){
        registerParser("text/html",Parser.JSON);
        if (estado.equals(Estados.ALEATORIO)){
            estado = Estados.random();
        }

        String responseCpf = given()
                .spec(RequestSpec.GetSpecCpfPost())
            .when()
                .param("cpf_estado", estado)
                .param("pontuacao", comPontuacao ? "S" : "N")
                .post()
            .then()
                .statusCode(200)
                .contentType("text/html")
                .extract()
                .body()
                .htmlPath().get().toString()
                ;

        String responseRg = given()
                .spec(RequestSpec.GetSpecRgPost())
            .when()
                .param("pontuacao", comPontuacao ? "S" : "N")
                .post()
            .then()
                .statusCode(200)
                .contentType("text/html")
                .extract()
                .body()
                .htmlPath().get().toString()
                ;

        String responsePisPasep = given()
                .spec(RequestSpec.GetSpecPisPasepPost())
            .when()
                .param("pontuacao", comPontuacao ? "S" : "N")
                .post()
            .then()
                .statusCode(200)
                .contentType("text/html")
                .extract()
                .body()
                .htmlPath().get().toString()
                ;

        String responseTituloEleitor = given()
                .spec(RequestSpec.GetSpecTituloEleitorPost())
            .when()
                .param("estado", estado)
                .post()
            .then()
                .statusCode(200)
                .contentType("text/html")
                .extract()
                .body()
                .htmlPath().get().toString()
                ;

        return DocumentosPessoaObj
                .builder()
                    .Cpf(responseCpf)
                    .Rg(responseRg)
                    .PisPasep(responsePisPasep)
                    .TituloEleitor(responseTituloEleitor)
                .build();
    }
}

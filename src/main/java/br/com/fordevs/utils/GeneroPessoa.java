package br.com.fordevs.utils;

import java.lang.String;

public enum GeneroPessoa {

    MASCULINO("M"),
    FEMININO("F"),
    ALEATORIO("I");

    private String descricao;

    GeneroPessoa(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}

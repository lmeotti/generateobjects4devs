package br.com.fordevs.utils;

public enum Estados {

    ALEATORIO(""),
    AC("AC"),
    AL("AL"),
    AM("AM"),
    AP("AP"),
    BA("BA"),
    CE("CE"),
    DF("DF"),
    ES("ES"),
    GO("GO"),
    MA("MA"),
    MG("MG"),
    MS("MS"),
    MT("MT"),
    PA("PA"),
    PB("PB"),
    PE("PE"),
    PI("PI"),
    PR("PR"),
    RJ("RJ"),
    RN("RN"),
    RS("RS"),
    RO("RO"),
    RR("RR"),
    SC("SC"),
    SE("SE"),
    SP("SP"),
    TO("TO");

    private java.lang.String descricao;

    Estados(java.lang.String descricao) {
        this.descricao = descricao;
    }

    public java.lang.String getDescricao() {
        return descricao;
    }

    public static Estados random() {
        return Estados.values()[Double.valueOf(
                Math.random() * Estados.values().length).intValue()];
    }
}

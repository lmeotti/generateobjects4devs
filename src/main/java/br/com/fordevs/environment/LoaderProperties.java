package br.com.fordevs.environment;

import org.apache.commons.configuration.PropertiesConfiguration;

public class LoaderProperties {
    private static PropertiesConfiguration config = loadProperties();

    private static PropertiesConfiguration loadProperties() {

        PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration();

            try {
                propertiesConfiguration.load("src/main/resources/application.properties");
            } catch (org.apache.commons.configuration.ConfigurationException e) {
                e.printStackTrace();
            }

        return propertiesConfiguration;
    }

    public static String getBaseUri(){
        return config.getString("url.base.uri");
    }

    public static String getBasePathUsers(){
        return config.getString("url.base.path.users");
    }

    public static String getParamAcaoPessoa(){ return config.getString("param.acao.gerar.pessoa"); }

    public static String getParamAcaoCpf(){ return config.getString("param.acao.gerar.cpf"); }

    public static String getParamAcaoRg(){
        return config.getString("param.acao.gerar.rg");
    }

    public static String getParamAcaoTituloEleitor(){
        return config.getString("param.acao.gerar.titulo.eleitor");
    }

    public static String getParamAcaoPisPasep(){
        return config.getString("param.acao.gerar.titulo.pis.pasep");
    }

    public static String getParamAcaoCnpj(){
        return config.getString("param.acao.gerar.cnpj");
    }
}

package br.com.fordevs.tests.apis;

import br.com.fordevs.specifications.RequestSpec;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class PessoasTests {

    private static RequestSpecification specification;
    
    @BeforeClass
    public void SetUp(){
        specification = RequestSpec.GetSpecPessoaPost();
    }
    
    @Test
    public void GerarPessoaGeneroMasculino(){
        given()
                .spec(specification)
            .when()
                .param("sexo","M")
                .param("pontuacao","N")
                .param("idade",0)
                .param("txt_qtde",1)
                .post()
            .then()
                .statusCode(200)
                .contentType("application/json")
        ;
    }

    @Test
    public void GerarPessoaGeneroFeminino(){
        given()
                .spec(specification)
            .when()
                .param("sexo","F")
                .param("pontuacao","N")
                .param("idade",0)
                .param("txt_qtde",1)
                .post()
            .then()
                .statusCode(200)
                .contentType("application/json")
        ;
    }

    @Test
    public void GerarPessoaGeneroAleatorio(){
        given()
                .spec(specification)
            .when()
                .param("sexo","I")
                .param("pontuacao","N")
                .param("idade",0)
                .param("txt_qtde",1)
                .post()
            .then()
                .statusCode(200)
                .contentType("application/json")
        ;
    }

    @Test
    public void GerarPessoaIdadeEspecifica(){
        given()
                .spec(specification)
            .when()
                .param("sexo","I")
                .param("pontuacao","N")
                .param("idade",19)
                .param("txt_qtde",1)
                .post()
            .then()
                .statusCode(200)
                .contentType("application/json")
            .assertThat()
                .body("idade", is(19))
        ;
    }

    @Test
    public void GerarPessoasIdadeEspecifica(){
        given()
                .spec(specification)
            .when()
                .param("sexo","I")
                .param("pontuacao","N")
                .param("idade",19)
                .param("txt_qtde",3)
                .post()
            .then()
                .statusCode(200)
                .contentType("application/json")
            .assertThat()
                .body("[0].idade", is(19))
            .and()
                .body("[1].idade", is(19))
            .and()
                .body("[2].idade", is(19))
        ;
    }

    @Test
    public void GerarPessoaIdadeMinima(){
        given()
                .spec(specification)
            .when()
                .param("sexo","I")
                .param("pontuacao","N")
                .param("idade",1)
                .param("txt_qtde",1)
                .post()
            .then()
                .statusCode(200)
                .contentType("application/json")
            .assertThat()
                .body("idade", is(1))
        ;
    }

    @Test
    public void GerarQntMaximaDePessoas(){
        JsonPath response =
            given()
                .spec(specification)
            .when()
                .param("sexo","I")
                .param("pontuacao","N")
                .param("idade",0)
                .param("txt_qtde",30)
                .post()
            .then()
                .statusCode(200)
                .contentType("application/json")
                .extract()
                    .body()
                    .jsonPath()
        ;

        Assert.assertEquals(response.getList("nome").size(), 30);
    }

    @Test
    public void GerarQntMaiorQuePermitidoDePessoas(){
        given()
                .spec(specification)
            .when()
                .param("sexo","I")
                .param("pontuacao","N")
                .param("idade",0)
                .param("txt_qtde",31)
                .post()
            .then()
                .statusCode(200)
                .contentType("application/json")
            .assertThat()
                .body("nome",is("Limite Máximo a ser gerado são 30 pessoas"))
        ;
    }

    @Test
    public void GerarPessoaComPontuacao(){
        given()
                .spec(specification)
            .when()
                .param("sexo","I")
                .param("pontuacao","S")
                .param("idade",0)
                .param("txt_qtde",1)
                .post()
            .then()
                .statusCode(200)
                .contentType("application/json")
            .assertThat()
                .body("cpf", stringContainsInOrder(".","-"))
            .and()
                .body("rg", stringContainsInOrder(".","-"))
            .and()
                .body("cep", stringContainsInOrder("-"))
            .and()
                .body("telefone_fixo", stringContainsInOrder("(",")","-"))
            .and()
                .body("celular", stringContainsInOrder("(",")","-"))
        ;
    }

    @Test
    public void GerarPessoaSemPontuacao(){
        given()
                .spec(specification)
            .when()
                .param("sexo","I")
                .param("pontuacao","N")
                .param("idade",0)
                .param("txt_qtde",1)
                .post()
            .then()
                .statusCode(200)
                .contentType("application/json")
            .assertThat()
                .body("cpf", not(stringContainsInOrder(".","-")))
            .and()
                .body("rg", not(stringContainsInOrder(".","-")))
            .and()
                .body("cep", not(stringContainsInOrder("-")))
            .and()
                .body("telefone_fixo", not(stringContainsInOrder("(",")","-")))
            .and()
                .body("celular", not(stringContainsInOrder("(",")","-")))
        ;
    }
}

package br.com.fordevs.tests.methods;

import br.com.fordevs.objects.PessoaObj;
import br.com.fordevs.services.Pessoa;
import br.com.fordevs.utils.GeneroPessoa;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GerarPessoaTests {

    @Test
    public void SemPontuacao(){
        PessoaObj pessoaGerada = Pessoa.Get(GeneroPessoa.ALEATORIO, false, 30);

        assertThat("CPF" ,pessoaGerada.getCpf(), not(stringContainsInOrder(".","-")));
        assertThat("RG" ,pessoaGerada.getRg(), not(stringContainsInOrder(".","-")));
        assertThat("CEP" ,pessoaGerada.getCep(), not(stringContainsInOrder("-")));
        assertThat("Celular" ,pessoaGerada.getCelular(), not(stringContainsInOrder("(",")","-")));
        assertThat("Telefone" ,pessoaGerada.getTelefone(), not(stringContainsInOrder("(",")","-")));
    }

    @Test
    public void ComPontuacao(){
        PessoaObj pessoaGerada = Pessoa.Get(GeneroPessoa.ALEATORIO, true, 30);

        assertThat("CPF" ,pessoaGerada.getCpf(), stringContainsInOrder(".","-"));
        assertThat("RG" ,pessoaGerada.getRg(), stringContainsInOrder(".","-"));
        assertThat("CEP" ,pessoaGerada.getCep(), stringContainsInOrder("-"));
        assertThat("Celular" ,pessoaGerada.getCelular(), stringContainsInOrder("(",")","-"));
        assertThat("Telefone" ,pessoaGerada.getTelefone(), stringContainsInOrder("(",")","-"));
    }

    @Test
    public void GeneroMasculino(){
        PessoaObj pessoaGerada = Pessoa.Get(GeneroPessoa.MASCULINO, true, 30);

        assertThat("CPF" ,pessoaGerada.getCpf(), stringContainsInOrder(".","-"));
        assertThat("RG" ,pessoaGerada.getRg(), stringContainsInOrder(".","-"));
        assertThat("CEP" ,pessoaGerada.getCep(), stringContainsInOrder("-"));
        assertThat("Celular" ,pessoaGerada.getCelular(), stringContainsInOrder("(",")","-"));
        assertThat("Telefone" ,pessoaGerada.getTelefone(), stringContainsInOrder("(",")","-"));
    }

    @Test
    public void GeneroFeminino(){
        PessoaObj pessoaGerada = Pessoa.Get(GeneroPessoa.FEMININO, true, 30);

        assertThat("CPF" ,pessoaGerada.getCpf(), stringContainsInOrder(".","-"));
        assertThat("RG" ,pessoaGerada.getRg(), stringContainsInOrder(".","-"));
        assertThat("CEP" ,pessoaGerada.getCep(), stringContainsInOrder("-"));
        assertThat("Celular" ,pessoaGerada.getCelular(), stringContainsInOrder("(",")","-"));
        assertThat("Telefone" ,pessoaGerada.getTelefone(), stringContainsInOrder("(",")","-"));
    }
}
